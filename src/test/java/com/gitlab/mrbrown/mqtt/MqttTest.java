package com.gitlab.mrbrown.mqtt;

import java.util.*;

import com.hivemq.client.mqtt.mqtt5.*;
import org.eclipse.paho.client.mqttv3.*;
import org.junit.jupiter.api.*;
import org.testcontainers.junit.jupiter.*;

import static java.nio.charset.StandardCharsets.UTF_8;

@Testcontainers
class MqttTest {

    @Container
    private MosquittoContainer mosqitto = new MosquittoContainer();

    private MqttClient subscriber;

    private List<String> messages = new ArrayList<>();


    @Test
    void testPaho() throws Exception {
        MqttClient client = new MqttClient(mosqitto.getAddress(), UUID.randomUUID().toString());
        client.connect();
        client.publish("world/paho", new MqttMessage("Hello World from Paho".getBytes(UTF_8)));
        client.disconnect();

        Thread.sleep(200);
        Assertions.assertFalse(messages.isEmpty());
        Assertions.assertEquals(messages.get(0), "Hello World from Paho");
    }

    @Test
    void testHiveMQ() throws Exception {
        Mqtt5BlockingClient client = Mqtt5Client.builder()
                .identifier(UUID.randomUUID().toString())
                .serverHost(mosqitto.getHost())
                .serverPort(mosqitto.getFirstMappedPort())
                .buildBlocking();

        client.connect();
        client.publishWith()
                .topic("world/hivemq")
                .payload("Hello World from HiveMQ".getBytes(UTF_8))
                .send();
        client.disconnect();

        Thread.sleep(200);
        Assertions.assertFalse(messages.isEmpty());
        Assertions.assertEquals(messages.get(0), "Hello World from HiveMQ");
    }


    @BeforeEach
    void setUp() throws Exception {
        subscriber = new MqttClient(mosqitto.getAddress(), UUID.randomUUID().toString());
        subscriber.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) {
            }

            @Override
            public void messageArrived(String t, MqttMessage m) throws Exception {
                messages.add(new String(m.getPayload()));
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken t) {
            }
        });

        subscriber.connect();
        subscriber.subscribe("#");
    }

    @AfterEach
    void tearDown() throws Exception {
        subscriber.disconnect();
        subscriber.close();
    }

}
