package com.gitlab.mrbrown.mqtt;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.HostPortWaitStrategy;

public class MosquittoContainer extends GenericContainer<MosquittoContainer> {

    public MosquittoContainer() {
        super("eclipse-mosquitto");

        this.addExposedPort(1883);
        this.waitingFor(new HostPortWaitStrategy());
    }

    public String getAddress() {
        final String host = this.getHost();
        final int port = this.getFirstMappedPort();
        return "tcp://" + host + ":" + port;
    }
}
