package com.gitlab.mrbrown.mqtt;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import javax.ejb.Singleton;

import org.eclipse.paho.client.mqttv3.*;

@Singleton
public class MqttPublisher {

    public void publish(String topic, String content) throws MqttException {
        MqttClient client = new MqttClient("tcp://mosquitto:1883", UUID.randomUUID().toString());

        client.connect();

        MqttMessage message = new MqttMessage(content.getBytes(StandardCharsets.UTF_8));
        client.publish(topic, message);

        client.disconnect();
    }

}
