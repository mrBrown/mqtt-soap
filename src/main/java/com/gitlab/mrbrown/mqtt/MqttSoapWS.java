package com.gitlab.mrbrown.mqtt;


import javax.inject.Inject;

import javax.ejb.Stateless;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.eclipse.paho.client.mqttv3.MqttException;

@Stateless
@WebService
public class MqttSoapWS {

    @Inject
    MqttPublisher mqttPublisher;

    public void pushToRabbitMQ(@WebParam(name="topic")  final String topic, @WebParam(name="content") final String content) throws MqttException {
        mqttPublisher.publish(topic,content);
    }
}
