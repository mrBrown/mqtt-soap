package com.gitlab.mrbrown.mqtt;

import java.time.Instant;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;

import org.eclipse.paho.client.mqttv3.*;

@Singleton
public class MqttScheduledPublisher {

    @Inject
    MqttPublisher mqttPublisher;

    @Schedule(second = "*/5", minute = "*", hour = "*", persistent = false)
    public void scheduled() throws MqttException {
        mqttPublisher.publish("/scheduled", "Hallo Welt: "+ Instant.now());
    }

}
