FROM maven:3 as build
WORKDIR /workspace/app

COPY pom.xml .
COPY src src

RUN mvn package -DskipTests

FROM tomee:8.0.2-plume
COPY --from=build /workspace/app/target/mqtt-soap.war webapps/mqtt-soap.war
